package de.jonas.android.schoolapp;

import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.view.MenuItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class VertretActivity extends Activity {

    public static final String INTENT_EXTRA_WEEK_NAME = "INTENT_EXTRA_WEEK_NAME";


    private WebView wvVertret;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.webview);

        if(!getIntent().hasExtra(INTENT_EXTRA_WEEK_NAME)) {
            Toast.makeText(this, "Kein Week 1 gefunden!", Toast.LENGTH_LONG).show();
            finish();
        }


        String strWeekName = getIntent().getStringExtra(INTENT_EXTRA_WEEK_NAME);;

        ActionBar actionBar = getActionBar();
        if(actionBar != null) {
            actionBar.setTitle(strWeekName);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        wvVertret = (WebView) findViewById(R.id.wvVertret);

        wvVertret.setWebViewClient(new WebViewClient());
        wvVertret.loadUrl("http://www.sn.schule.de/~ms25dd/vertretung/" + strWeekName.toLowerCase() + ".html");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId() == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }
}
