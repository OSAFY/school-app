package de.jonas.android.schoolapp;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;


public class MainActivity extends Activity implements AdapterView.OnItemClickListener{

    private String[] straWeekNames;
    private ArrayAdapter<String> stringArrayAdapter;
    private DrawerLayout Drawer;
    private ActionBarDrawerToggle Toggle;
    public static final String BUNDLE_NAME_KEY = "Name";

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Bundle bundleOne = new Bundle();
        bundleOne.putString(BUNDLE_NAME_KEY, "Woche 1");


        Drawer = (DrawerLayout) findViewById(R.id.dlDrawerLayout);
        Toggle = new ActionBarDrawerToggle(this, Drawer, R.drawable.ic_drawer, R.string.open, R.string.close);

        Drawer.setDrawerListener(Toggle);

        getActionBar().setHomeButtonEnabled(true);
        getActionBar().setDisplayHomeAsUpEnabled(true);



        straWeekNames = new String[] {"Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag"};

        ListView lvListView = (ListView) findViewById(R.id.lvList);

        stringArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, straWeekNames);
        lvListView.setAdapter(stringArrayAdapter);
        lvListView.setOnItemClickListener(this);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        Toggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        Toggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (Toggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        if(!isNetworkAvailable()) {
            Toast.makeText(this, "Kein Internet!", Toast.LENGTH_LONG).show();
            return;
        }

        Intent intent = new Intent(this, VertretActivity.class);
        intent.putExtra(VertretActivity.INTENT_EXTRA_WEEK_NAME, straWeekNames[i]);
        startActivity(intent);


    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        return networkInfo != null && networkInfo.isConnected();
    }
}
